#!/bin/bash
echo "running apt get"
sudo apt-get update
sudo apt-get install python3-pip npm
echo "running npm"
sudo npm cache clean -f
sudo npm install -g n
sudo n stable
sudo npm -g install nodemon

echo "running pip3"
##if you have any problems try without --user
pip3 install --user pycrypto
pip3 install --user pycrypto-on-pypi
pip3 install --user pycryptodome
pip3 install --user flask flask_cors jsonpickle

