from hashlib import sha256
import json
import time
from transaction import *


CAPACITY = 2


class Block:
	def __init__(self, index, transactions, previous_hash):
		self.genesis = False
		self.previous_hash = previous_hash
		self.timestamp = -1 #default value, takes its price on mining
		self.nonce = 0 #solution of proof of work
		self.index = index #incr number of each block
		self.transactions = [transactions] #list of transactions in block 
		self.current_hash = 0 #default value, takes its final price on mining

	
	def to_dict(self):
		b = {}
		b['genesis'] = self.genesis
		b['previous_hash'] = self.previous_hash
		b['timestamp'] = self.timestamp 
		b['nonce'] = self.nonce
		b['index'] = self.index
		b['transactions'] = self.transactions #already dict
		b['current_hash'] = self.current_hash
		return b



	def hash_block(self):
		#Create SHA-256 hash for the block
		block_json = json.dumps(self.__dict__, sort_keys = True).encode()
		return hashlib.sha256(block_json).hexdigest()




