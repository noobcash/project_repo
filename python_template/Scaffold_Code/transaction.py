from collections import OrderedDict
import json
import binascii
from Crypto.Hash import SHA256
from Crypto.PublicKey import RSA
from Crypto.Signature import pkcs1_15
import hashlib



class Transaction:

    def __init__(self, sender_address, receiver_address, amount, t_in, t_out):
        self.sender_address = sender_address  #exported public key of sender
        self.receiver_address = receiver_address  #exported public key of receiver
        self.amount = amount  
        self.transaction_inputs = t_in  #list of Transaction Input 
        self.transaction_outputs = t_out  #list of Transaction Output 
        
        self.id = self.hash_transaction() #hash of transaction with t_out = [] and signature = 0
        self.signature = 0 


    def to_dict(self):
    	t = {}
    	t['sender_address'] = self.sender_address 
    	t['receiver_address'] = self.receiver_address 
    	t['amount'] = self.amount
    	t['transaction_inputs'] = self.transaction_inputs #transaction_inputs is already a dict
    	t['transaction_outputs'] = self.transaction_outputs #transaction_outputs is already a dict
    	t['id'] = self.id
    	t['signature'] = binascii.hexlify(self.signature).decode('ascii')
    	return t


    def hash_transaction(self):
        #Create SHA-256 hash for the transaction. signature is added afterwords
        message = {}
        message['amount'] = self.amount
        message['transaction_inputs'] = self.transaction_inputs
        message = json.dumps(message, sort_keys = True).encode()
        return hashlib.sha256(message).hexdigest()


    def sign_transaction(self, private_key):
        #Sign transaction with private key. Use only the following fields of transaction to sign it
        message = {}
        message['amount'] = self.amount
        message['transaction_inputs'] = self.transaction_inputs
        message['transaction_outputs'] = self.transaction_outputs 
        message = json.dumps(message, sort_keys = True).encode()
        hashed_message = SHA256.new(message)
        signature = pkcs1_15.new(private_key).sign(hashed_message)
        return signature          
  

    
class Transaction_output:
    
    def __init__(self, coins, address_target, origin_t_id):
        self.receiver = address_target
        self.coins = coins
        self.origin_t_id = origin_t_id
        self.id = self.hash_transaction()
 
    def hash_transaction(self):
        #Create SHA-256 hash for the transaction.
        transaction_json = {}
        transaction_json['coins'] = self.coins,
        transaction_json['origin_t_id'] = self.origin_t_id,
        transaction_json = json.dumps(transaction_json)
        return hashlib.sha256(transaction_json.encode()).hexdigest()

    
    def to_dict(self):
    	t = {}
    	t['receiver'] =  self.receiver
    	t['coins'] = self.coins
    	t['origin_t_id'] = self.origin_t_id
    	t['id'] = self.id
    	return t

