from block import *
from wallet import *
from transaction import *
from Crypto.PublicKey import RSA
import datetime
import requests
from multiprocessing.dummy import Pool


CAPACITY = 2
MINING_DIFFICULTY = 1
PARTICIPANTS = 3
CHAIN_TRANSACTIONS = 10
pool = Pool(100)


def import_key(key):
	return RSA.importKey(binascii.unhexlify(key.encode('ascii')))

def export_key(key):
	return binascii.hexlify(key.exportKey(format='DER')).decode('ascii')

def hash_block(block):
	#Create SHA-256 hash for the block
	block_json = json.dumps(block, sort_keys = True).encode()
	return hashlib.sha256(block_json).hexdigest()


#t is json except from keys
def verify_transaction(t):
    sender_address = import_key(t['sender_address'])
    message = {}
    message['amount'] = t['amount']
    message['transaction_inputs'] = t['transaction_inputs']
    message['transaction_outputs'] = t['transaction_outputs']
    message = json.dumps(message, sort_keys = True).encode()
    hashed_message = SHA256.new(message)
    try:
        pkcs1_15.new(sender_address).verify(hashed_message, binascii.unhexlify(t['signature'].encode('ascii')))
        return True
    except (ValueError):
        print("Invalid verification")
        return False
  

def validate_block(block, prev_block):
	if block['genesis']: #dont validate
		print("validate_block: genesis block")
		return 1
	#check current hash
	current_hash = block['current_hash']
	timestamp = block['timestamp']
	block['current_hash'] = 0 #as it was on mining
	block['timestamp'] = -1 #as it was on mining
	new_hash = hash_block(block)
	#current hash could go wrong only for received-from-broadcast blocks
	if (new_hash != current_hash):
		print("validate_block: current_hash changed! invalid block")
		return -1

	#check previous hash
	previous_hash = block['previous_hash']
	last_chain_hash = prev_block['current_hash'] #new block is gonna be added at the chain-tail
	if (last_chain_hash != previous_hash):
		print("validate_block: previous_hash changed! invalid block")
		return 0

	block['current_hash'] = current_hash #fix block as it was
	block['timestamp'] = timestamp	

	print("validate_block: previous and current hash ok! valid block")
	return 1



def valid_chain(chain):
	#check for the longer chain accroose all nodes
	for i in range(len(chain)-1):
		prev_b = chain[i]
		b = chain[i+1]
		if validate_block(b, prev_b) != 1:
			return False
	return True



def receive_chain(ip, port, length):
	new_chain = []

	for i in range(length):
		url = "http://" + ip + ":" + str(port) + '/block?index=' + str(i)
		response = requests.get(url)
		if (response.status_code != 200):
			print("error in broadcast")
			return False

		block = response.json()['block']
		new_chain.append(block)

	new_chain.sort(key=lambda x: x['index'], reverse=False)
	if valid_chain(new_chain):
		print("receive_chain: valid chain")
		return new_chain
	else :
		print("receive_chain: OUPS! invalid chain")
		return []




class Node_things:
	def __init__(self, id, ip, port, balance):
		self.id = id #we dont use it anywhere but anyway
		self.ip = ip
		self.port = port
		self.balance = balance



class Node:
	def __init__(self, id, bootstrap): #bootstrap is block
		#self.NBCs = 100;
		##set

		self.chain = []#[bootstrap]
		self.id = id
		self.wallet = 0 #wallet <-> node
		self.utxo = {} #selft.utxo is a dictionary: {'wallet': list_of_utxo}
		self.ring = {}   #here we store information for every node, as its id, its address (ip:port) its public key and its balance 
		self.last_block = None
		#self.chain_transactions = [] #latest 10 transactions included in the chain


	#bootstrap node calls it for every wallet at initialization
	#key is exported, node is dict
	def register_node_to_ring(self, pubkey, node):
		#add this node to the ring, only the bootstrap node can add a node to the ring after checking his wallet and ip:port address
		#bottstrap node informs all other nodes and gives the request node an id and 100 NBCs
		self.ring[pubkey] = node #added to dict



	#create a list for every wallet. called after rings are ok
	#takes exported key
	def init_utxo_list(self, pubkey):
		self.utxo[pubkey] = []


	def create_new_block(self, t, index, previous_hash):
		block = Block(index, t, previous_hash) #nonce = -1 just for init, timestamp = -1, right timestamp when added to chain
		return block.to_dict()


	#only bootstrap calls this on the beging
	#extra function because we shouldn't validate this
	#receiver is exported key
	def create_genesis_transaction(self, receiver_address, amount):
		print("mesa stin genesis transaction tha skasei")
		gen_key = RSA.generate(2048)
		t = Transaction(export_key(gen_key.publickey()), receiver_address, amount, [], []) #sender_addr = 0, couldnt make this so generated a gen_key
		#t.id = t.hash_transaction()
		t_out = Transaction_output(amount, receiver_address, t.id)
		t_out = t_out.to_dict()
		t.transaction_outputs.append(t_out)
		t.signature = t.sign_transaction(gen_key)
		t = t.to_dict()

		#fix wallets, ring and utxos
		self.wallet.balance += amount
		self.wallet.transactions.append(t)
		self.utxo[receiver_address].append(t_out)
		self.ring[receiver_address]['balance'] += amount
		self.add_transaction_to_block(t)
		return t


	#create a wallet for this node, with a public key and a private key
	#every node <-> one wallet
	def create_wallet(self):
		print("create wallet yeayy")
		key = RSA.generate(2048)
		wallet = Wallet(key.publickey(), key)
		self.wallet = wallet
		print("key",key)
		return key


	#sender receiver are exported keys
	def create_transaction(self, sender_address, receiver_address, amount): 
		#if not enoug coins in sender's wallet
		if self.ring[sender_address]['balance'] < amount: 
			print("create_transaction: transaction creation aborted, not enough coins!")
			return False
		else :
			t_in, t_in_sum = self.calc_t_in(sender_address, amount)
			#t_id is now calculated. It is equal to the hash of t, when t has empty t_out but proper t_in.
			#t_in are unique so t_id is unique
			t = Transaction(sender_address, receiver_address, amount, t_in, [])
			t.transaction_outputs = self.calc_t_out(sender_address, receiver_address, amount, t_in_sum, t.id)
			sender_private_key = self.wallet.get_private_key()
			t.signature = t.sign_transaction(sender_private_key)
			t = t.to_dict()

			#update wallets
			self.wallet.balance -= amount
			self.wallet.transactions.append(t)
			#update balance array
			self.ring[sender_address]['balance'] -= amount
			self.ring[receiver_address]['balance'] += amount

			print("create_transaction: transaction created!")
			self.add_transaction_to_block(t)
			self.broadcast_transaction(t)

			return t 




	#calculates t_out and also fixes utxos of sender(if there is change) and receiver. 
	#returns t_out list for transaction obj
	def calc_t_out(self, sender_address, receiver_address, amount, t_in_sum, origin_t_id):
		t_out = []
		#t_out for sender, if there is change
		if t_in_sum > amount: 
			change = t_in_sum - amount
			t = Transaction_output(change, sender_address, origin_t_id)
			t = t.to_dict()
			t_out.append(t)
			self.utxo[sender_address].append(t) #update utxo

		#t_out for receiver
		t = Transaction_output(amount, receiver_address, origin_t_id)
		t = t.to_dict()
		t_out.append(t)
		self.utxo[receiver_address].append(t) #update utxo

		return t_out




	#called if transaction can take place
	#calculates transaction_input and fixes utxos of sender 
	#returns transaction_input list and their sum
	def calc_t_in(self, sender_address, amount):
		#sender_utxo = self.utxo[sender_address]
		t_in_sum = 0
		t_in = []

		for utxo in self.utxo[sender_address]: #sender_utxo:
			t_in_sum += utxo['coins']
			t_in.append(utxo)
			#sender_utxo.remove(utxo) #keeps the remaining utxo
			self.utxo[sender_address].remove(utxo)
			if t_in_sum >= amount:
				break

		#update the utxos
		#self.utxo[sender_address] = sender_utxo

		return t_in, t_in_sum



	"""def broadcast_transaction(self, transaction):
		data = json.dumps(transaction)
		for addr, node_stf in self.ring.items():
			if (import_key(addr) != self.wallet.public_key):
				ip = node_stf['ip']
				port = node_stf['port']
				url = "http://" + ip + ":" + str(port) + '/transactions?transaction=' + data
				r = requests.get(url)
				if(r.status_code != 200):
					print("error in broadcast")
					return False
		return True
"""
	def broadcast_transaction(self, transaction):
		results = []
		data = json.dumps(transaction)
		for addr, node_stf in self.ring.items():
			if (import_key(addr) != self.wallet.public_key):
				ip = node_stf['ip']
				port = node_stf['port']
				url = "http://" + ip + ":" + str(port) + '/transactions?transaction=' + data
				results.append(pool.apply_async(requests.get(url)))
		"""print(results)
		for r in results:
			r = r.get()
			if(r.status_code != 200):
				print("error in broadcast")
				return False"""
		return True


	##transaciotn in the json form sent from an other node
	def receive_broadcast_transaction(self, t):
		t = json.loads(t)
		#print('transaction broadcasted:', t)
		ret = self.validate_transaction(t)
		if ret:
			self.add_transaction_to_block(t)
		return ret




	#called when transaction is recieved from broadcast
	def validate_transaction(self, t):
		sender_address = t['sender_address'] #the exported key (der)
		receiver_address = t['receiver_address'] #the exported key (der)
		amount = t['amount'] 

		if not verify_transaction(t):
			print("validate_transaction: invalid transaction signature verification")
			return False

		#check that alice has the coins in her wallet		
		if self.ring[sender_address]['balance'] < amount:
			print("validate_transaction: sender's balance not enough for transaction")
			return False

		#if checks ok, fix utxos, balances and add trans to block
		t_in, t_in_sum = self.calc_t_in(sender_address, amount)
		t_out = self.calc_t_out(sender_address, receiver_address, amount, t_in_sum, t['id'])
		
		#update balance array
		self.ring[sender_address]['balance'] -= amount
		self.ring[receiver_address]['balance'] += amount
		#If this transaction affects my wallet:
		if (import_key(t['receiver_address']) == self.wallet.public_key):
			self.wallet.balance += amount
			self.wallet.transactions.append(t)

		#add to block
		print("validate_transaction: valid transaction, add it to block")		
		return True 


	

	def add_transaction_to_block(self, t):
		#if enough transactions  mine
		print("mesa stin transaction to block")
		if not self.chain: #if empty
			print("empty chain")
			b = self.create_new_block(t, 0, 1) #index=0, previous_hash=1, nonce=0 (default)
			print("created new block")
			if not t['transaction_inputs']: #if genesis, panta tha bainei edw giati se olous to 1o block einai to genesis
				print("an einai genesis")
				b['genesis'] = True
				self.last_block = b
				self.mine_block(b) #it wont mine, it will broadcast it and the others wont validate
		else:	
			#block = self.chain[-1]
			block = self.last_block
			#if block['genesis'] is True: #leave the genesis block alone
			if block is None:
				print("add_transaction_to_block: sth went WRONG with last_block")
				return False
			if block['genesis'] is True: #leave the genesis block alone
				print("add_transaction_to_block: genesis before, add new block with t")
				b = self.create_new_block(t, block['index']+1, block['current_hash'])
				self.last_block = b
			else : 
				mine_flag = self.add_transaction(block, t) #flag=0: added. flag=1: added + mine. flag=-1: no space
				if mine_flag == 1:
					print("add_transaction_to_block: t added to block and time to mine")
					self.mine_block(block)	
				elif mine_flag == -1 :
					print("add_transaction_to_block: no space, add new block with t")
					b = self.create_new_block(t, block['index']+1, block['current_hash']) #the final hash of block, when it has all its continents (trans basically)
					self.last_block = b
				else :
					print("add_transaction_to_block: t added to block")
		#self.update_chain_transactions(t, None)


	#taken from block class:
	def add_transaction(self, b, t):
		#add a transaction to the block
		if len(b['transactions']) < CAPACITY-1:
			b['transactions'].append(t)
			print("add_transaction: added to block")
			return 0
		elif len(b['transactions']) == CAPACITY-1:
			b['transactions'].append(t)
			print("add_transaction: time to mine")
			return 1
		else :
			print("add_transaction: no space in this block")
			return -1



	def mine_block(self, block):
		if block['genesis']: #dont mine
			block['timestamp'] = str(datetime.datetime.now())
			self.chain.append(block) 
			self.broadcast_block(block)
		else:
			#proof-of-work
			#the one who finds the right nonce, broadcast block
			nonce = 0;
			block_hash = hash_block(block) 
			
			while self.valid_proof(block_hash) is False:
				nonce += 1
				block_hash = hash_block(block)  

			block['nonce'] = nonce
			#block.current_hash takes new price here (only), it was 0 at init
			block['current_hash'] = block_hash 
			block['timestamp'] = str(datetime.datetime.now())
			print("mine_block: finish! hash block is ", block_hash)
			#self.chain.append(block)  
			if validate_block(block, self.chain[-1]) == 1:
				self.chain.append(block) #when validated: appended
				self.broadcast_block(block)
			#self.last_block = block ############################etsi den prepei? hmm hanw omws pragmata pou borei na eixa sto last block
			
		

	def valid_proof(self, block_hash, difficulty=MINING_DIFFICULTY):
		#return block_hash[:difficulty] == '0'*difficulty
		valid = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']
		return (block_hash[0] in valid)


	"""def broadcast_block(self, block):
		data = json.dumps(block)
		for addr, node_stf in self.ring.items():
			if (import_key(addr) != self.wallet.public_key):
				ip = node_stf['ip']
				port = node_stf['port']
				url = "http://" + ip + ":" + str(port) + '/blocks?block=' + data
				r = requests.get(url)
				if(r.status_code != 200):
					print("error in broadcast")
					return False
		return True"""

	def broadcast_block(self, block):
		results = []
		data = json.dumps(block)
		for addr, node_stf in self.ring.items():
			if (import_key(addr) != self.wallet.public_key):
				ip = node_stf['ip']
				port = node_stf['port']
				url = "http://" + ip + ":" + str(port) + '/blocks?block=' + data
				results.append(pool.apply_async(requests.get(url)))
		"""print(results)
		for r in results:
			r = r.get()
			if(r.status_code != 200):
				print("error in broadcast")
				return False"""

		return True



	def receive_broadcast_block(self, block):
		block = json.loads(block)
		if block['genesis']: #not sure if this must be here or in validate_block
			self.last_block = block
			prev_block = []
		else :
			prev_block = self.chain[-1]
		ret = validate_block(block, prev_block)
		if ret == 0:
			resolved = self.resolve_conflicts()
			while not resolved: #try until you get a valid chain
				resolved = self.resolve_conflicts()

		elif ret == 1:
			self.chain.append(block) #when validated: appended

		print('block broadcasted:', block)
		return ret


	#concencus functions

	def resolve_conflicts(self):
		#resolve correct chain
		print("resolve_conflicts: Time to resolve conflicts")
		new_chain = False
		max_length = len(self.chain)
        
        #find if someone has longer chain
		for addr, node_stf in self.ring.items():
			if (import_key(addr) != self.wallet.public_key):
				ip = node_stf['ip']
				port = node_stf['port']
				url = "http://" + ip + ":" + str(port) + '/chain_length' 
				response = requests.get(url)
				if (response.status_code != 200):
					print("error in broadcast")
					return False

				length = response.json()['length']

				if length > max_length: 
					max_length = length
					max_ip = ip
					max_port = port
					new_chain = True

		#if there is someone with longer chain, ask it from him
		if new_chain:
			print("resolve_conflicts: found longer chain")
			self.chain = receive_chain(max_ip, max_port, max_length)
			if not self.chain:
				return False
			print("resolve_conflicts: chain fixed")
		#else, i have the longer chain
		else:
			new_chain = self.chain
			print("resolve_conflicts: my chain was the longest")

		#Resolve utxo-balances anyway. Loose the last_block's transactions in the name of simplicity.
		self.resolve_utxo_balance(new_chain)
		self.last_block = self.chain[-1]
		return True



	def resolve_utxo_balance(self, chain):
		for addr,utxo in self.utxo.items():
			self.utxo[addr] = []
		for b in chain:
			for t in b['transactions']:
				for t_in in t['transaction_inputs']:
					self.utxo[t_in['receiver']].remove(t_in)
				for t_out in t['transaction_outputs']:
					self.utxo[t_out['receiver']].append(t_out)

		for addr,utxo in self.utxo.items():
			coins = 0
			for t_out in utxo:
				coins += t_out['coins']
			self.ring[addr]['balance'] = coins 

		print('resolve_utxo_balanc: done')

		



#den me peirazei apla na kanw abort giati meta to validate block tha treksw thn resolve conflicts h opoia tha ftiajei utxo kai balance
	"""def check_last_block_transactions(self, b):
		for t in self.last_block['transactions']:
			for t_in_b in b['transactions']:
				if t['id'] == t_in_b['id']:
					self.last_block['transactions'].remove(t)
					break



	def update_chain_transactions(self, t, b):
		if b:
			for tr in b['transactions']:
				self.chain_transactions.append(tr['id'])
		elif t:
			self.chain_transactions.append(t['id'])

		if len(self.chain_transactions) > CHAIN_TRANSACTIONS:
			self.chain_transactions = self.chain_transactions[:CHAIN_TRANSACTIONS] #remove first elements

		return True

"""
