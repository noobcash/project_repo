#import requests
from flask import Flask, jsonify, request, render_template
from flask_cors import CORS


from block import *
from wallet import *
from transaction import *
from node import *


PARTICIPANTS = 2
BOOTSTRAP_IP = '192.168.0.1'
BOOTSTRAP_PORT = 5000



### JUST A BASIC EXAMPLE OF A REST API WITH FLASK



app = Flask(__name__)
CORS(app)
#blockchain = Blockchain()


#.......................................................................................



# get all transactions in the blockchain
@app.route('/transactions', methods=['GET'])
def get_transactions(self):
    ##transactions = blockchain.transactions
	transaction = request.args.get('transaction')
	print(transaction)
    	##response = {'transactions': 'gamiesai'}
	self.receive_broadcast_transaction(self,transaction)
    	return jsonify('mpla'), 200 ## ??


@app.route('/blocks', methods=['GET'])
def get_blocks(self):
    ##transactions = blockchain.transactions
	block = request.args.get('block')
	print(block)
    	##response = {'transactions': 'gamiesai'}
	self.receive_broadcast_block(self,block)
    	return jsonify('mpla'), 200 ## ??


#mono o bootstrap tis exei aytes

@app.route('/register')
def register_node():
	node_stf = request.args.get('node_stf')
	pubkey = request.args.get('pubkey')
	print(node_stf)
	print(pubkey)
	bootstrap.register_node_to_ring(node_stf, pubkey)



@app.route('/init_bootstrap')
def init_bootstrap():
	bootstrap = Node(0, [])
	my_key = bootstrap.create_wallet()
	#print(my_key)
	node = Node_things(bootstrap.id, host, port, 100*PARTICIPANTS)
	bootstrap.register_node_to_ring(my_key.publickey(), node)
	t = bootstrap.create_genesis_transaction(my_key.publickey(), 100*PARTICIPANTS)
	bootstrap.add_transaction_to_block(t) 

	r = str(bootstrap.ring[my_key.publickey().exportKey('OpenSSH')].port)
	response = json.dumps(r)
	return jsonify(response), 200



@app.route('/genesis_transactions')
def genesis_transactions():
	t_gen = []
	for addr, node in bootstrap.ring.items():
		if addr == my_key.publickey() is False:
			#no validation needed
			t = bootstrap.create_genesis_transaction(my_key.publickey(), addr, 100*PARTICIPANTS)
			#utxo = 
	


#call this at bootstrap after genesis and ring to all
@app.route('/first_transactions')
def first_transactions():
	ts = []
	ring = bootstrap.ring
	bootstrap_pubkey = bootstrap.wallet.public_key
	for addr, node_stf in bootstrap.ring.items():
		if addr == bootstrap_pubkey is False:
			t = bootstrap.create_transaction(bootstrap_pubkey, addr, 100)
			ts.append(t)
	response = json.dumps(ts)
	return jsonify(response), 200



@app.route('/create_wallet')
def create_wallet():
	node = Node(1, [])
	my_key = node.create_wallet()
	node_stf = Node_things(node.id, host, port, 0)

	#send this to host=localhost, port=5000 (req=register)
	data = json.dumps(node_stf.__dict__)
	url = BOOTSTRAP_IP + ":" + BOOTSTRAP_PORT + '/register?node_stf=' + str(data) + "&pubkey=" + str(my_key.public_key())
	r = requests.get(url)
	if(r.status_code!=200):
		print("error in broadcast")
		return False





#broadcast-recievebroadcast for ring and genesis block of bootstrap


@app.route('/create_transaction')
def create_transaction(reciever, amount):
	sender = node.wallet.public_key
	t = node.create_transaction(sender, reciever, amount)
	response = json.dumps(t)
	return jsonify(response), 200



@app.route('/myr')
def m():
	print("Hey")
	response = {'transactions': "transactions"}
	return jsonify(response), 200

# run it once for every node

if __name__ == '__main__':
    from argparse import ArgumentParser

    parser = ArgumentParser()
    parser.add_argument('-p', '--port', default=5000, type=int, help='port to listen on')
    args = parser.parse_args()
    port = args.port

    app.run(host = 'localhost', port = port)
