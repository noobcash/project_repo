from flask import Flask, jsonify, request, render_template
from flask_cors import CORS
from block import *
from wallet import *
from transaction import *
from node import *
from collections import namedtuple
from multiprocessing.dummy import Pool


BOOTSTRAP_IP = '192.168.0.1'
BOOTSTRAP_PORT = 5000
node = None
host = 0
port = 0
node_index = 0


app = Flask(__name__)
CORS(app)


#.......................................................................................
#check that receive_chain function works
@app.route('/receive_chain')
def receive_chain_():
	chain = receive_chain(BOOTSTRAP_IP, BOOTSTRAP_PORT, 3)
	response = {
		'chain': chain
	}
	return jsonify(response), 200


#this is called len(chain) times from node who asks for the chain
@app.route('/block')
def get_block():
	global node
	index = int(request.args.get('index'))
	response = {
        'block': node.chain[index],
    }
	return jsonify(response), 200


#send amount to ip-port wallet
@app.route('/create_transaction')
def create_transaction():
	amount = int(request.args.get('amount'))
	ip = request.args.get('ip')
	port = int(request.args.get('port'))
	ring = node.ring
	node_pubkey = export_key(node.wallet.public_key)
	t = None
	for addr, node_stf in node.ring.items():
		if node_stf['ip'] == ip and node_stf['port'] == port:
			t = node.create_transaction(node_pubkey, addr, amount)
			break;
	if t:
		response = {
			'transaction': t
		}
	else:
		response = "No such ip - port or not enough coins"
	return jsonify(response), 200


#view the contents of last_block
@app.route('/my_last_block')
def get_my_last_block():
	global node
	response = {
        'last_block': node.last_block,
    }
	return jsonify(response), 200


#get the transactions of my wallet
@app.route('/get_my_transactions')
def get_my_transactions():
	global node
	response = {
        'transactions': node.wallet.transactions,
    }
	return jsonify(response), 200


#get my balance
@app.route('/get_my_balance')
def get_my_balance():
	global node
	response = {
        'balance': node.wallet.balance,
    }
	return jsonify(response), 200


#get ip-port for every participant
@app.route('/get_ip_port')
def get_ring():
	global node
	ring = node.ring
	ip_port_list = []
	for addr, node_stf in node.ring.items():
		ip = node_stf['ip']
		port = node_stf['port']
		ip_port_list.append({'ip':str(ip)+':'+str(port)})

	response = {
        'ip_port': ip_port_list,
    }
	return jsonify(response), 200


#view my chain
@app.route('/chain')
def get_chain():
	global node
	response = {
        'chain': node.chain,
    }
	return jsonify(response), 200


#called from resolve_conflicts when a node needs to find 
#out the node with the longest chain
@app.route('/chain_length')
def get_chain_length():
	global node
	response = {
        'length': len(node.chain),
    }
	return jsonify(response), 200


#view my ring and utxo lists 
@app.route('/get_ring_utxo')
def get_ring_utxo():
	global node
	response = {
		'ring': node.ring,
		'utxo': node.utxo
	}
	return jsonify(response), 200


#called when a node broadcasts a transaction
@app.route('/transactions', methods=['GET'])
def get_transactions():
	global node 
	transaction = request.args.get('transaction')
	#print("rest GET TRANSTACTION  ", transaction)
	node.receive_broadcast_transaction(transaction)
	return jsonify('received transaction'), 200


#called when a node broadcasts a block
@app.route('/blocks', methods=['GET'])
def get_blocks():
	global node
	block = request.args.get('block')
	#print(block)
	node.receive_broadcast_block(block)
	return jsonify('received block'), 200


#called when bootstrap broadcasts ring-utxo (/broadcast_ring_utxo) 
#after genesis transaction
@app.route('/receive_ring_utxo', methods= ['GET'])
def receive_ring_from_bootstrap():
	global node
	node.ring  = json.loads(request.args.get('ring'))
	node.utxo = json.loads(request.args.get('utxo'))
	#print("ring ",ring," utxo ", utxo)
	return jsonify("receive broadcast ring ok"), 200


#BOOTSTRAP METHOD
#broadcasts ring and utxo to others so everyone to now things (ip, port, pubkey) of everyone
@app.route('/broadcast_ring_utxo')
def broadcast_ring():
	global node
	
	ring = json.dumps(node.ring)
	utxo = json.dumps(node.utxo)
	
	for addr, node_stf in node.ring.items():
		if addr != export_key(node.wallet.public_key):
			ip = node_stf['ip']
			port = node_stf['port']
			url = "http://" + ip + ":" + str(port) + '/receive_ring_utxo?ring=' + ring + '&utxo=' + utxo
			r = requests.get(url)
			if(r.status_code != 200):
				print("error in broadcast")
				return jsonify("error broadcast ring"), r.status_code
	return jsonify("broadcast ring ok"), 200



#BOOTSTRAP METHOD
#first thing to happen, init of bootstrap
@app.route('/init_bootstrap')
def init_bootstrap():
	global node
	global node_index

	node = Node(node_index, [])
	node_index += 1
	my_key = node.create_wallet()
	node_stf = Node_things(node.id, host, port, 0)
	print(export_key(my_key.publickey()))
	node.register_node_to_ring(export_key(my_key.publickey()), node_stf.__dict__)
	node.init_utxo_list(export_key(my_key.publickey()))

	return jsonify("init bootstrap ok"), 200



#BOOTSTRAP METHOD
@app.route('/genesis_transaction')
def genesis_transaction():
	global node
	print("mesa stin genesis")
	#this broadcasts genesis block to others as well
	t = node.create_genesis_transaction(export_key(node.wallet.public_key), 100*PARTICIPANTS)
	response = {
		'genesis_transactions': t,
	}
	return jsonify(t), 200


#BOOTSTRAP METHOD
#bootstrap calls this after genesis and ring to all
@app.route('/first_transactions') 
def first_transactions():
	ts = []
	ring = node.ring
	bootstrap_pubkey = export_key(node.wallet.public_key)
	for addr, node_stf in node.ring.items():
		if addr != bootstrap_pubkey:
			t = node.create_transaction(bootstrap_pubkey, addr, 100)
			ts.append(t)
	response = {
		'first_transactions': ts,
	}
	return jsonify(response), 200


#BOOTSTRAP METHOD
#registers a new wallet. called from /create_wallet
@app.route('/register')
def register_node():
	global node

	#node_stf = jsonpickle.decode(request.args.get('node_stf'))
	node_stf = json.loads(request.args.get('node_stf'))
	pubkey = request.args.get('pubkey')
	print(pubkey)
	node.register_node_to_ring(pubkey, node_stf)
	node.init_utxo_list(pubkey)
	return jsonify("register ok"), 200


#called when someone wants to join
@app.route('/create_wallet')
def create_wallet():
	global host
	global port
	global node
	global node_index

	node = Node(node_index, [])
	node_index += 1
	my_key = node.create_wallet()
	node_stf = Node_things(node.id, host, port, 0)

	data = json.dumps(node_stf.__dict__)
	url = "http://" + BOOTSTRAP_IP + ":" + str(BOOTSTRAP_PORT) + '/register?node_stf=' + data + "&pubkey=" + export_key(my_key.publickey())
	r = requests.get(url)
	#print (r)
	if (r.status_code != 200):
		#print("error in create wallet")
		return jsonify("error create wallet"), r.status_code
	else :
		return jsonify("create wallet"), 200


#test function
@app.route('/additional_transaction')
def additional_transaction():
	ts = []
	ring = node.ring
	node_pubkey = export_key(node.wallet.public_key)
	for addr, node_stf in node.ring.items():
		if addr != node_pubkey:
			t = node.create_transaction(node_pubkey, addr, 10)
			ts.append(t)
	response = {
		'transaction': ts
	}
	return jsonify(response), 200


"""@app.route('/get_my_chain_transactions')
def get_my_chain_transactions():
	global node
	response = {
        'transactions': node.chain_transactions,
    }
	return jsonify(response), 200
"""


# run it once for every node
if __name__ == '__main__':
    
    from argparse import ArgumentParser

    parser = ArgumentParser()
    parser.add_argument('-p', '--port', default=5000, type=int, help='port to listen on')
    parser.add_argument('-f', '--host', default='localhost', type=str, help='port to listen on')
    args = parser.parse_args()
    port = args.port
    #host = 'localhost'
    host = args.host
    app.run(host = host, port = port)
