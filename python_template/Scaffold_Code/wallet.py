import binascii
import Crypto
import Crypto.Random
from Crypto.Hash import SHA
from Crypto.PublicKey import RSA
from Crypto.Signature import PKCS1_v1_5
import hashlib
import json
from time import time
from urllib.parse import urlparse
from uuid import uuid4



class Wallet:

	def __init__(self, public_key, private_key): #keys given from user who creates wallet
		self.public_key = public_key
		self.__private_key = private_key #private member
		self.address = self.public_key
		self.transactions = []
		self.balance = 0


	def get_private_key(self):
		return self.__private_key
