const express = require('express');
var fs = require('fs')
var http = require('http')
var app = express();
app.set('view engine', 'ejs');
app.use("/includes", express.static(__dirname + '/views/includes'));
app.use("/views", express.static(__dirname + '/views'));
app.get('/', function(req, res) {
    res.render('front', {
        root: __dirname + '/views'
    });
});

http.createServer({
    //key: fs.readFileSync('server.key'),
    //cert: fs.readFileSync('server.cert')
}, app).listen(3000, function() {
    console.log('Server running on http://localhost:3000/')
})
